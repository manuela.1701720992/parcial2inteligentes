import os
import shutil
import random

def normalizar_cantidad_imagenes(ruta_datos, categorias, limite_por_categoria):
    # Mapeo de nombres de categorías a números
    mapeo_categorias = {'glioma': '0', 'meningioma': '1', 'notumor': '2', 'pituitary': '3'}

    for categoria in categorias:
        carpeta_categoria = os.path.join(ruta_datos, categoria)
        imagenes_categoria = os.listdir(carpeta_categoria)
        print(f"Cantidad de imágenes antes de la mezcla en {categoria}: {len(imagenes_categoria)}")
        random.shuffle(imagenes_categoria)
        print(f"Cantidad de imágenes después de la mezcla en {categoria}: {len(imagenes_categoria)}")

        # Crear una carpeta para almacenar las imágenes normalizadas
        carpeta_destino = os.path.join(ruta_datos, "normalized", mapeo_categorias[categoria])
        os.makedirs(carpeta_destino, exist_ok=True)

        # Seleccionar aleatoriamente un número específico de imágenes
        imagenes_seleccionadas = imagenes_categoria[:limite_por_categoria]

        # Copiar las imágenes seleccionadas a la carpeta de destino
        for imagen in imagenes_seleccionadas:
            origen = os.path.join(carpeta_categoria, imagen)
            destino = os.path.join(carpeta_destino, imagen)
            print(f"Copiando {origen} a {destino}")
            shutil.copy(origen, destino)

if __name__ == "__main__":
    # Configuración de rutas y parámetros
    ruta_datos = "cleaned/Testing/"
    categorias = ['glioma', 'meningioma', 'notumor', 'pituitary']
    limite_por_categoria = 330 # Número mínimo de imágenes

    # Normalizar la cantidad de imágenes por categoría
    normalizar_cantidad_imagenes(ruta_datos, categorias, limite_por_categoria)