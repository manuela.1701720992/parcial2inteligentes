Análisis e interpretación de los resultados
Modelo 1:
La precisión del modelo en el conjunto de entrenamiento ha aumentado progresivamente, alcanzando un 99.95% en la última época. Esto indica que el modelo ha aprendido bien las características y patrones en los datos de entrenamiento
La función de pérdida disminuye considerablemente a medida que avanzan las épocas, lo cual es un indicativo positivo.
Estas métricas muestran la habilidad del modelo para clasificar cada clase. Un alto precision, recall y F1-Score indican que el modelo tiene un buen rendimiento en la tarea de clasificación de imágenes de resonancia magnética cerebral en diferentes categorías.
La matriz de confusión muestra que el modelo tiene un buen rendimiento en la mayoría de las clases, con algunas dificultades en la identificación de gliomas y meningiomas.

Modelo 2:
 A diferencia del modelo anterior, este modelo incorpora cuatro capas convolucionales, cada una seguida por una capa de MaxPooling. Esto permite una representación jerárquica más profunda de las características de la imagen.
Se ha utilizado la función de activación "sigmoid" en todas las capas convolucionales en lugar de "relu".
La precisión (accuracy) es baja, alrededor del 25%, lo que indica que el modelo no ha aprendido de manera efectiva a clasificar las imágenes en las categorías objetivo.
La pérdida (loss) no parece disminuir significativamente durante el entrenamiento.
La precision y recall para todas las clases son 0.0. Esto indica que el modelo no ha logrado acertar positivos verdaderos (precision) ni capturar todos los positivos reales (recall) para ninguna de las clases.
La matriz de confusión muestra que el modelo predice la misma clase para todas las imágenes, probablemente asignando una única clase dominante.
Aunque las 4 capas otorgan cierta profundidad, la elección de la función de activación "sigmoid" en lugar de "relu" puede haber contribuido al problema, ya que "sigmoid" puede tener problemas con el entrenamiento de redes profundas.

Modelo 3:
Cuatro capas convolucionales (capa_1, capa_2, capa_3, capa_4) con funciones de activación "relu" y capas de MaxPooling correspondientes.
La capa de salida utiliza la función de activación "softmax", típica para problemas de clasificación.
El número de filtros aumenta progresivamente en las capas convolucionales (16, 32, 64, 256). Esto permite que la red capture características más complejas y abstractas a medida que profundiza.
La precisión, la precisión (precision) y la recuperación (recall) en los resultados del entrenamiento son bastante altos, indicando un aprendizaje efectivo.
La pérdida (loss) disminuye significativamente, lo que indica que el modelo está mejorando a medida que avanza el entrenamiento.
La precisión en los datos de prueba es alta, alcanzando alrededor del 94%.
La matriz de confusión muestra un buen rendimiento en la clasificación de las clases, con algunos errores en la predicción.


Modelo 4:
Utilice una combinación de funciones de activación "ReLU" y "Sigmoid" en diferentes capas convolucionales. Esperando que esto diera lugar a una representación más rica y compleja de las características.
La cantidad de épocas la aumente para observar el comportamiento.
El modelo muestra un desempeño muy pobre durante el entrenamiento y la prueba, con una precisión de aproximadamente el 25%. Esto sugiere que el modelo no está aprendiendo patrones significativos en los datos.
La pérdida (loss) no muestra una disminución significativa a lo largo de las épocas, lo cual indica que el modelo no está mejorando.
Las métricas de precisión, recall y F1-score son cero, lo que confirma la falta de aprendizaje.
Se observó que utilizando en la  segunda capa convolucional la función de activación"Sigmoid", lo cual pudo afectar la capacidad del modelo para aprender patrones complejo ya que esta función tiende a usarse en la capa de salida para problemas de clasificación binaria.

Modelo 5 VGG:
Se utiliza la arquitectura preentrenada VGG19 desde keras.applications como base_model. 
Se añaden capas densas personalizadas al final de la arquitectura VGG19 para adaptar el modelo a la tarea específica de clasificación de imágenes médicas. Esto incluye una capa de aplanamiento (Flatten), una capa densa con activación ReLU y otra capa densa de salida con activación Softmax.
Este modelo utiliza imágenes a color con 3 canales (RGB).
Muestra una capacidad de generalización limitada, ya que la precisión en las pruebas es moderada (48.58%).
La puntuación F1 es razonable (53.26%), indicando un equilibrio entre precisión y recuperación.
La matriz de confusión revela áreas de confusión entre las clases, con errores significativos en algunas predicciones.
Posiblemente si se ajustaran los hiperparámetros o cambios en la arquitectura podría mejorar.


Realizar un análisis comparativo sobre las matrices de confusión de los mejores 2 modelos según sus criterios y argumente cuál debería ser elegido como el final para una fase de despliegue (3 y 1):
Modelo 3(El mejor):
Precisión y F1 Score Elevados: El Modelo 3 exhibe una precisión y un F1 Score superiores, lo que indica una capacidad más robusta para clasificar correctamente las imágenes, minimizando tanto los falsos positivos como los falsos negativos.
Priorización de la Clasificación Precisa: Si la máxima prioridad es lograr una clasificación precisa y reducir al mínimo los errores de clasificación, el Modelo 3 se presenta como la elección preferida.


Modelo 1:
Rendimiento Sólido: Aunque el Modelo 1 tiene un rendimiento ligeramente inferior en términos de precisión y F1 Score, sigue siendo un modelo sólido y competente.
Equilibrio entre Rendimiento y Estabilidad: En situaciones donde se busca un equilibrio entre el rendimiento general y la estabilidad del modelo, el Modelo 1 puede ser una opción viable. Puede ser especialmente útil si se consideran otros factores, como la complejidad del modelo y los recursos computacionales disponibles.



Implemente el algoritmo “Grad Cam” y en el informe coloque 2 ejemplos de predicción con Grad Cam Original y Categoría:
Modelo 1:
Predicción Correcta: El modelo 1 predice correctamente la imagen cargada, que pertenece a la categoría "notumor". La predicción confiada de probabilidad cercana a 1 para la clase correcta sugiere que el modelo ha aprendido de manera efectiva las características distintivas asociadas con esta categoría.
Grad-CAM: El mapa de activación generado por Grad-CAM se centra en las regiones de la imagen que el modelo considera más importantes para realizar la predicción. En este caso, al centrarse en las áreas relevantes para "notumor", el modelo demuestra estar atendiendo a las características significativas asociadas con esta clase.


Modelo 3:
Predicción Correcta: Similar al Modelo 1, el Modelo 3 también realiza una predicción correcta para la imagen cargada, que pertenece a la categoría "meningioma". La alta probabilidad asignada a la clase correcta indica la eficacia del modelo en reconocer patrones distintivos.
Grad-CAM: El mapa de activación generado por Grad-CAM para el Modelo 3 se centra en las áreas relevantes para la categoría "meningioma". Esto respalda la idea de que el modelo está prestando atención a características específicas que son cruciales para clasificar correctamente las imágenes de esta clase.



Recomendaciones basadas en el hallazgo del experimento
Brinde una conclusión donde argumente según lo obtenido en el punto lo que usted considera que el modelo se fija por categoría para dar la predicción.

El modelo 3, el cual fue el que dio mejor resultado, se enfoca en características específicas de cada categoría al realizar predicciones. La atención a detalles distintivos de meningioma, en el caso prueba para el Grad-CAM , sugiere que el modelo se especializa en patrones relevantes para cada clase.
La alta precisión y recall indican que el modelo sopesa cuidadosamente las características únicas de cada categoría.


Plantee 1 conclusiones del resultado del proceso de experimentación (tenga como referencia las métricas obtenidas).
El Modelo 3 destaca por su rendimiento superior en términos de métricas clave como precisión y F1 Score. Si la prioridad principal es lograr la máxima precisión en la clasificación de imágenes médicas, el Modelo 3 sería la elección recomendada.
Mencione los posibles escenarios donde podría llegar a fallar el modelo elegido.
La elección de ciertos parámetros de la red, como el número de filtros en las capas convolucionales (16, 32, 64, 256), podría haber sido subóptima. Un número de filtros demasiado grande puede conducir a un sobreajuste, especialmente si la cantidad de datos de entrenamiento es limitada.
Si hay un desbalance significativo en la cantidad de datos disponibles para cada clase, el modelo puede tener dificultades para aprender patrones de clases minoritarias. Esto puede resultar en una predicción sesgada hacia las clases mayoritarias y una menor capacidad para generalizar en situaciones donde las clases minoritarias son críticas
Exponga que problemas encontró al momento de resolver el problema de clasificación y la manera de abordarlos, a su vez comente que consideraciones o técnicas tuvo en cuenta para evitar el de sobre ajuste (overfitting) o sub ajuste (underfitting).

Para mitigar el riesgo de sobreajuste, se implementaron estrategias como la normalización de datos, asegurándose de que cada categoría contara con la misma cantidad de muestras en todas las carpetas. Este enfoque busca equilibrar la representación de clases y proporcionar al modelo una distribución más uniforme durante el entrenamiento. En cuanto al subajuste, aunque la duplicación de datos mediante técnicas como girar imágenes podría mejorar la generalización, las limitaciones de tiempo de ejecución y la capacidad del equipo impidieron su implementación exhaustiva. Esta restricción influyó en la capacidad para abordar completamente el subajuste, y la demora en la ejecución de la red también se convirtió en un factor a considerar en el proceso.
